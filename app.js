var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var drugController = require('./contollers/drug')

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true,
}));

mongoose.connect("mongodb://localhost:27017/drugDB");

app.get('/drug', drugController.all);
app.get('/drug/:name', drugController.find);
app.post('/drug/', drugController.create);

app.listen(8000, function() {
    console.log("Сервер начал работу");
});