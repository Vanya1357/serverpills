var Drug = require('../models/drug');

exports.create = function(req, res) {
    var name = req.body.name;
    var price = req.body.price;
    var rating = req.body.rating;
    var drug = {
        name: name,
        price: price,
        rating: rating,
    }
    Drug.create(drug, function(err, doc) {
        if (err) {
            console.log(err);
            return res.sendStatus(500);
        }
        res.sendStatus(200);
    })
}

exports.all = function(req, res) {
    Drug.all(function(err, docs) {
        if (err) {
            console.log(err);
            return res.sendStatus(500);
        }
        res.send(docs);
    })
}

exports.find = function(req, res) {
    Drug.find(req.params.name, function(err, docs) {
        if (err) {
            console.log(err);
            return res.sendStatus(500);
        }
        res.send(docs);
    })
}
