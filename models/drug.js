var mongoose = require("mongoose");
var Schema = mongoose.Schema;
mongoose.Promise = global.Promise;


var drugScheme = new Schema({
    name: {
        type: String,
        required: true,
    },
    price: Number,
    rating: Number,
},
{ versionKey: false });
var Drug = mongoose.model("Drug", drugScheme);

exports.all = function(cb) {
    Drug.find({}, function(err, docs) {
        cb(err, docs);
    })
}

exports.create = function(drug, cb) {
    Drug.create(drug, function(err, doc) {
        cb(err, doc);
    })
}

exports.find = function(name, cb) {
    Drug.find({name: { "$regex": name, "$options": "i"} }, function(err, docs) {
        cb(err, docs);
    })
}
